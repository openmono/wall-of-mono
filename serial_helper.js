#!/usr/bin/env node
var fs = require("fs");
var currentSource = null;

var updateFile = function(file)
{
	if (file != currentSource)
	{
		fs.unlink("./mono-serial", function(){
			console.log("creating new symlink for: "+file);
			currentSource = file;
			fs.symlinkSync("/dev/"+file,"./mono-serial");
		});
	}
}

var scanDir = function(files)
{
	var newest = null;
	var regex = /^cu\.usbmodem([0-9]+)$/;
	for (var f in files)
	{
		// if (files[f] == "cu.mono-serial")
		// 	continue;

		if (regex.test(files[f]))
		{
			//console.log("Found file: "+files[f])
			var match = regex.exec(files[f]);
			if (newest == null)
			{
				newest = Number(match[1]);
				continue;
			}
			else if (Number(match[1]) > newest)
			{
				newest = Number(match[1])
			}
		}
	}

	if (newest == null)
		return null;

	//console.log("Newest is: cu.usbmodem"+newest);

	return "cu.usbmodem"+newest;
}

var checkFiles = function()
{

	//console.log("Checking for new files...");
	var nFile;

	fs.readdir("/dev",function(err, files){
		nFile = scanDir(files);
		if (nFile == null)
			return;

		updateFile(nFile);
	});
}

var timer = setInterval(checkFiles,100)
console.log("begin");