#include "app_controller.h"
using namespace mono::geo;
using mono::display::Color;
using mono::media::BMPImage;

#define COLOR_STEP_TIME_MS 100
#define MOCKUP_START_TIME_MS 50000
#define MOCKUP_STOP_TIME_MS 8000

static char const * fileName[] =
{
    "Hello",
    "Hue",
    "Calender",
    "Clock",
    "mandelbrot",
    "ToDo",
    "Weather",
    0
};

Toucher::Toucher (AppController * ctrl_)
:
    ctrl(ctrl_)
{
}

void Toucher::RespondTouchEnd(TouchEvent & event)
{
    if (TouchEvent::TOUCH_END == event.EventType)
        ctrl->handleTouch(event.Position);
}

AppController::AppController()
:
    state(COLOUR_CYCLE),
    colourTimer(COLOR_STEP_TIME_MS),
    mockupTimer(MOCKUP_START_TIME_MS),
    toucher(this),
    currentColor(BlackColor),
    goalColor(BlackColor),
    fs(SD_SPI_MOSI,SD_SPI_MISO,SD_SPI_CLK,SD_SPI_CS,"sd"),
    info(Rect(0, 100, 176, 25), "")
{
    info.setAlignment(TextLabelView::ALIGN_CENTER);
    mockupIndex = 0;
    colourTimer.setCallback(this,&AppController::updateScreen);
    colourTimer.Start();
    mockupTimer.setCallback(this,&AppController::changeMockup);
    mockupTimer.Start();
}

void AppController::handleTouch (geo::Point const & point)
{
    changeMockup();
    setupRandomNumbers();
}

void AppController::setScreenColor ()
{
    uint16_t random = rand();
    printf("new colour %4x\r\n",random);
    goalColor = Color(random);
}

void AppController::updateScreen ()
{
    bg.show();
    if (currentColor == goalColor) setScreenColor();
    else stepTowardsGoalColor();
    bg.setBackgroundColor(currentColor);
    bg.scheduleRepaint();
}

void AppController::stepTowardsGoalColor ()
{
    // Red
    uint8_t currentRed = currentColor.Red() >> 3;
    uint8_t goalRed = goalColor.Red() >> 3;
    if (currentRed < goalRed) ++currentRed;
    else if (currentRed > goalRed) --currentRed;
    // Green
    uint8_t currentGreen = currentColor.Green() >> 2;
    uint8_t goalGreen = goalColor.Green() >> 2;
    if (currentGreen < goalGreen) ++currentGreen;
    else if (currentGreen > goalGreen) --currentGreen;
    // Blue
    uint8_t currentBlue = currentColor.Blue() >> 3;
    uint8_t goalBlue = goalColor.Blue() >> 3;
    if (currentBlue < goalBlue) ++currentBlue;
    else if (currentBlue > goalBlue) --currentBlue;
    // Update
    currentColor = Color(currentRed<<3,currentGreen<<2,currentBlue<<3);
}

void AppController::changeMockup ()
{
    if (COLOUR_CYCLE == state) showMockup();
    else stopMockup();
}

void AppController::showMockup ()
{
    state = MOCKUP;
    colourTimer.Stop();
    mockupTimer.setInterval(MOCKUP_STOP_TIME_MS);
    String path = String::Format("/sd/mono/mockups/%s.bmp",fileName[mockupIndex]);
    printf("Load img: %s\r\n",path());
    FILE *tf = fopen(path(), "r");
    if (tf == 0)
    {
        debug("image file [%s.bmp] not found!\r\n",fileName[mockupIndex]);
        info.setBackground(currentColor);
        info.setText(String::Format("No: %s.bmp", fileName[mockupIndex]));
        info.show();
        fclose(tf);
        return;
    }
    fclose(tf);
    image = BMPImage(path);
    if (!image.IsValid())
        printf("Failed to load image!\r\n");
    iv = ImageView(&image);
    iv.setRect(Rect(0, 0, 176, 220));
    iv.show();
}

void AppController::stopMockup ()
{
    state = COLOUR_CYCLE;
    colourTimer.Start();
    mockupTimer.setInterval(MOCKUP_START_TIME_MS);
    nextMockupIndex();
}

void AppController::nextMockupIndex ()
{
    ++mockupIndex;
    if (0 == fileName[mockupIndex]) mockupIndex = 0;
}

void AppController::setupRandomNumbers ()
{
    uint32_t unique[2] = {0,0};
    CyGetUniqueId((uint32_t*)&unique);
    uint32_t seed = unique[0] ^ unique[1] ^ rand();
    srand(seed);
    printf("seed %4lx (%4lx %4lx)\r\n",seed,unique[1],unique[0]);
}

void AppController::monoWillGotoSleep ()
{
    printf("GoToSleep\r\n");
    colourTimer.Stop();
    mockupTimer.Stop();
}

void AppController::monoWakeFromReset ()
{
    printf("WakeFromReset\r\n");
}

void AppController::monoWakeFromSleep ()
{
    mono::IApplicationContext::SoftwareResetToApplication();
    printf("WakeFromSleep\r\n");
    colourTimer.Start();
    mockupTimer.Start();
}
