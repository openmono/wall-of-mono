#ifndef app_controller_h
#define app_controller_h
#include <mono.h>
#include <SDFileSystem.h>

using namespace mono;
using namespace mono::ui;
using namespace mono::display;

class AppController;

class Toucher : public mono::TouchResponder
{
    AppController * ctrl;
public:
    void RespondTouchEnd (mono::TouchEvent &);
    Toucher (AppController *);
};

class AppController : public mono::IApplication
{
    enum {COLOUR_CYCLE, MOCKUP} state;
    Timer colourTimer;
    Timer mockupTimer;
    BackgroundView bg;
    Toucher toucher;
    Color currentColor;
    Color goalColor;
    SDFileSystem fs;
    size_t mockupIndex;
    mono::media::BMPImage image;
    ImageView iv;
    TextLabelView info;
    void setScreenColor ();
    void stepTowardsGoalColor ();
    void updateScreen ();
    void changeMockup ();
    void showMockup ();
    void stopMockup ();
    void nextMockupIndex ();
    void setupRandomNumbers ();
public:
    AppController ();
    void monoWakeFromReset ();
    void monoWillGotoSleep ();
    void monoWakeFromSleep ();
    void handleTouch (geo::Point const & point);
};

#endif /* app_controller_h */
